package HW20.utils;

import jakarta.persistence.Persistence;
import jakarta.persistence.EntityManagerFactory;

public class PersistenceUtil {
    private static PersistenceUtil instance;
    private static EntityManagerFactory entityManagerFactory;

    private PersistenceUtil() {
        entityManagerFactory = Persistence.
                createEntityManagerFactory("default");
    }

    public static PersistenceUtil getInstance() {
        if (instance == null)
            instance = new PersistenceUtil();
        return instance;
    }

    private EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public static EntityManagerFactory obtainEntityManagerFactory() {
        return getInstance().getEntityManagerFactory();
    }

    public static void closeEntityManagerFactory() {
        getInstance().getEntityManagerFactory().close();
    }
}
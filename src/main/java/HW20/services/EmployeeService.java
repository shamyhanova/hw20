package HW20.services;

import HW20.dao.Dao;
import HW20.dao.EmployeeDao;
import HW20.entities.Employee;

import java.util.List;

public class EmployeeService {
    private static Dao<Employee> employeeDao;

    public EmployeeService() {
        employeeDao = new EmployeeDao();
    }

    public void create(Employee employee) {
        employeeDao.createEntityManager();
        employeeDao.create(employee);
        employeeDao.closeEntityManager();
    }

    public Employee retrieve(Integer id) {
        employeeDao.createEntityManager();
        Employee employee = employeeDao.retrieve(id);
        employeeDao.closeEntityManager();
        return employee;
    }

    public List<Employee> retrieveAll() {
        employeeDao.createEntityManager();
        List<Employee> employees = employeeDao.retrieveAll();
        employeeDao.closeEntityManager();
        return employees;
    }

    public Employee update(Employee employee) {
        employeeDao.createEntityManager();
        Employee updated = employeeDao.update(employee);
        employeeDao.closeEntityManager();
        return updated;
    }

    public boolean delete(Integer id) {
        employeeDao.createEntityManager();
        boolean deleted = employeeDao.delete(id);
        employeeDao.closeEntityManager();
        return deleted;
    }
}

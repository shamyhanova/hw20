package HW20.dao;

import jakarta.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

public interface Dao<T extends Serializable> {

    public EntityManager createEntityManager();

    public void closeEntityManager();

    public void create(T entity);

    public T retrieve(Integer id);

    public List<T> retrieveAll();

    public T update(T entity);

    public boolean delete(Integer id);
}

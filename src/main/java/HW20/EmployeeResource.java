package HW20;

import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import HW20.entities.Employee;
import HW20.services.EmployeeService;

import java.net.URI;

@Path("/employees")
public class EmployeeResource {
    private EmployeeService employeeService = new EmployeeService();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(@Context UriInfo uriInfo,
                           @Valid Employee employee) {
        employeeService.create(employee);
        URI uri = UriBuilder.fromUri(uriInfo.getRequestUri())
                .path("{id}").build(employee.getId());
        return Response.created(uri).entity(employee).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Integer id) {
        Employee employee = employeeService.retrieve(id);
        if (employee == null)
            return Response.status(400, "Can not found").build();
        return Response.ok().entity(employee).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {
        return Response.ok().entity(employeeService
                .retrieveAll()).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Integer id,
                           @Valid Employee employee) {

        employee.setId(id);
        if (employeeService.update(employee) == null)
            return Response.status(400, "Can not found").build();
        return Response.ok().entity(employee).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Integer id) {
        if (!employeeService.delete(id))
            return Response.status(400, "Can not found").build();
        return Response.ok().build();
    }
}